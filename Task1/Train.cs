﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal struct Train
    {
        public string destination;
        public DateTime departureTime;
        public int numTrain;
    }
}
