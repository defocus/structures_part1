﻿using System;

namespace Task1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Train[] schedule = new Train[8];

            FillSchedule(schedule);

            SortSchedule(schedule);

            Console.Write("Enter number train for search: ");

            int numTrain = int.Parse(Console.ReadLine());

            Console.WriteLine(SearchTrain(numTrain, schedule));

            Console.ReadKey();
        }

        private static string SearchTrain(int numTrain, Train[] schedule)
        {
            foreach (var item in schedule)
                if (item.numTrain == numTrain)
                    return $"\n\tNumber train: \t\t{item.numTrain}\n" +
                        $"\tDestination train: \t{item.destination}\n" +
                        $"\tDeparture time: \t{item.departureTime}\n";

            return "Searched train not found";
        }

        private static void FillSchedule(Train[] schedule)
        {
            for (int i = 0; i < schedule.Length; i++)
            {
                Console.Write("Enter number train: ");

                schedule[i].numTrain = int.Parse(Console.ReadLine());

                Console.Write("Enter departure time(dd.mm.yyyy hh:mm): ");

                schedule[i].departureTime = DateTime.Parse(Console.ReadLine());

                Console.Write("Enter destination: ");

                schedule[i].destination = Console.ReadLine();

                Console.WriteLine(new string('-', 50));
            }
        }

        private static void ShowAllSchedule(Train[] schedule)
        {
            foreach (var item in schedule)
            {
                Console.WriteLine(item.numTrain);
                Console.WriteLine(item.departureTime);
                Console.WriteLine(item.destination);
            }
        }

        private static void SortSchedule(Train[] schedule)
        {
            Train temp;
            
            for(int i = 0; i < schedule.Length - 1; i++)
                for(int j = 0; j < schedule.Length - 1; j++)
                {
                    if(schedule[j].numTrain > schedule[j + 1].numTrain)
                    {
                        temp = schedule[j];
                        schedule[j] = schedule[j + 1];
                        schedule[j + 1] = temp;
                    }
                }
        }
    }
}
