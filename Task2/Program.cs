﻿using System;

namespace Task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            MyClass myClass = new MyClass();

            ClassTaker(myClass);

            MyStruct myStruct1;

            myStruct1.change = "One variant";

            MyStruct myStruct2 = new MyStruct();

            StructTaker(myStruct2);

            Console.WriteLine(myClass.change);

            Console.WriteLine(myStruct1.change);

            Console.WriteLine(myStruct2.change);
        }

        static void ClassTaker(MyClass myClass)
        {
            myClass.change = "Chanched";
        }

        static void StructTaker(MyStruct myStruct)
        {
            myStruct.change = "Chanched";
        }
    }
}
